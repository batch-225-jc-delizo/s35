// Packages
const express = require("express");
const mongoose = require("mongoose");

// Configuration
const app = express();
const port = 5000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// MongoDB Connection
mongoose.connect("mongodb+srv://jcdelizo:pogepoge12345@cluster0.y6g90au.mongodb.net/s35activity?retryWrites=true&w=majority", 
	{	
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

	// Connection Notifications
	let db = mongoose.connection;
	db.on("error", console.error.bind(console, "Connection Error"));
	db.on('open', () => console.log("Connected to MongoDB!"));

// Schemas
const signupSchema = new mongoose.Schema({ 
	
	username : String,
	password: String
});

// Models
const Signup = mongoose.model("Signup", signupSchema);

app.post('/signup', (req, res) => {
	Signup.findOne({username : req.body.username}, (err, result) => {

		if(result != null && result.username == req.body.username){

			return res.send("Duplicate user found");

		} else {

			let newSignup = new Signup({
				username : req.body.username,
				password : req.body.password

			});
			newSignup.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user created");
				}
			})
		}

	})
});

app.get("/signup", (req, res) => {
	Signup.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result			
			})

		}

	})
});

app.listen(port, () => console.log(`Server running at port ${port}!`))